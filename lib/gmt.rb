# This class provides a native extension accessing the programs of
# the {https://docs.generic-mapping-tools.org Generic Mapping Tools};
# an open source collection of about 80 command-line tools for
# manipulating geographic and Cartesian data sets (including filtering,
# trend fitting, gridding, projecting, etc.) and producing PostScript
# illustrations ranging from simple x–y plots via contour maps to
# artificially illuminated surfaces and 3D perspective views
#
# An example of usage:
#
#    require 'gmt'
#
#    common = { file: 'output.ps', R: '0/3/0/2', J: 'x1i' }
#
#    GMT.session do |gmt|
#      gmt.psxy('dots.xy', position: :first, S: 'c0.50c', G: 'blue', **common)
#      gmt.psxy('dots.xy', position: :last, S: 'c0.25c', G: 'red', **common)
#    end
#
# Those familiar with GMT will recognise the +-R+ (range) and +-J+ (projection)
# options which are now keys for the program options hash.
#
# == The session
#
# All GMT functions (modules in the GMT parlance) require a session context,
# and an instance of the GMT class is exactly such a context.
#
#    gmt = GMT.new
#
# There are no arguments for the constructor.
#
# A session object is also yielded by the {GMT.session} class method:
#
#    GMT.session do |gmt|
#      # do something with gmt
#    end
#
# == GMT functions
#
# Each GMT function is available as instance method, and each has the same
# signatures: several _arguments_, typically input files, followed by a
# single options hash.  The options hash corresponds to the command-line
# options of the GMT program; hence the shell command
#
#    gmt makecpt -Cgebco depths.txt -i2 -Z -E24 > bath.cpt
#
# would be replicated by the Ruby
#
#    gmt = GMT.new
#    gmt.makecpt('depths.txt', C: 'gebco', i: 2, Z: nil, E: 24, :> => 'bath.cpt')
#
# Note that
# - the argument(s) (the input file <code>'depths.txt'</code>) preceed(s)
#   the options hash
# - the options hash has keys which are symbols
# - options which lack arguments correspond to hash keys with nil values
#   <code>Z: nil</code>.
# - the output redirection is _also_ treated as hash option, with key
#   <code>:></code> and value which is the output file.
#
# The options without argument can also be specifed by a symbol
# _argument_, but recall that arguments must proceed the options hash,
# so the above would be
#
#    gmt.makecpt('depths.txt', :Z, C: 'gebco', i: 2, E: 24, :> => 'bath.cpt')
#
# using this format.
#
# One occasionally has need for multiple calls to the same option: the
# <code>-B</code> option being the usual culprit. In this case, use a single
# <code>:B</code> key and an array of the arguments:
#
#    B: [
#      'xa%g+l%s' % [dx, x_label],
#      'ya%g+l%s' % [dy, y_label],
#      'nWeS'
#     ], ...
#
# for example.
#
# == PostScript functions
#
# When creating complex PostScript plots one needs to make several calls to
# GMT functions, to use the +-O+ and +-K+ options, and to append output of
# the second and subsequent calls (rather than overwriting). This is tiresome
# and error-prone on the command-line, and more so in the "options hash"
# representation in the Ruby module.
#
# So we add some syntactic sugar for these PostScript functions: if the
# options hash has the keys +:position+ (with values one of +:first+, +:middle+,
# or +:last+) and +:file+ (with the value of the ouptut file), then the
# +-O+ and +-K+ options and the output redirection are handled by the class.
# So one might write
#
#     gmt = GMT.new
#     gmt.psbasemap( ..., file: 'map.ps', position: :first)
#     gmt.pstext(    ..., file: 'map.ps', position: :middle)
#     gmt.psxy(      ..., file: 'map.ps', position: :middle)
#     gmt.pslogo(    ..., file: 'map.ps', position: :last)
#
class GMT

  class << self

    # @param [Integer] major The major version
    # @param [Integer] minor The minor version
    # @param [Integer] release The release version
    # @return [Boolean] +true+ if the current API version is at least as requested
    #
    # @example If the current API version if 5.3.2, then
    #   GMT.version_at_least?(5) #=> true
    #   GMT.version_at_least?(5, 3, 1) #=> true
    #   GMT.version_at_least?(5, 3, 3) #=> false
    def version_at_least?(major, minor=0, release=0)
      if VERSION_MAJOR > major then
        true
      elsif VERSION_MAJOR < major then
        false
      else
        version_at_least_minor?(minor, release)
      end
    end

    private

    def version_at_least_minor?(minor, release)
      if VERSION_MINOR > minor then
        true
      elsif VERSION_MINOR < minor then
        false
      else
        version_at_least_release?(release)
      end
    end

    def version_at_least_release?(release)
      VERSION_RELEASE >= release
    end

    # @macro [attach] wrapper_ps
    #
    #   A PostScript function (accepts +:file+ and +:position+ options).
    #   @method $1(*files, **options)
    #   @param [Array<String>] files The input files
    #   @param [Hash] options The GMT command-line options in hash form
    #   @return [Boolean] +true+ on success
    #   @see https://docs.generic-mapping-tools.org/latest/$1.html
    def wrapper_ps(method)
      define_method(method) do |*args, **opts|
        args, opts = arg_opt_normalise(args, opts)
        coerce_postscript_options!(opts)
        coerce_append_option!(opts)
        self.send(api_function(method), *args, options_as_pairs(opts))
      end
    end

    # @macro [attach] wrapper_other
    #   @method $1(*arguments, **options)
    #   @param [Array<String>] arguments The arguments
    #   @param [Hash] options The GMT command-line options in hash form
    #   @return [Boolean] +true+ on success
    #   @see https://docs.generic-mapping-tools.org/latest/$1.html
    def wrapper_other(method)
      define_method(method) do |*args, **opts|
        args, opts = arg_opt_normalise(args, opts)
        coerce_append_option!(opts)
        self.send(api_function(method), *args, options_as_pairs(opts))
      end
    end

    public

    # @yield [gmt] yields the {GMT} session to the block
    def session
      gmt = GMT.new
      yield gmt
      gmt.free
    end

  end

  private

  # the name of the C function

  def api_function(method)
    [method.to_s, 'c'].join('_').to_sym
  end

  # convert symbol arguments to nil-valued options, and
  # call :to_s on the remaining arguments, so
  #
  #   arg_opt_normalise(['a', :b, Pathname('/c')], {d: 'e'})
  #
  # becomes
  #
  #   [['a', '/c'], {b: nil, d: 'e'}]
  #
  # returns the normalised arguments and options, does not
  # modify the inputs

  def arg_opt_normalise(args, opts)
    syms, strs = args.partition { |x| x.is_a? Symbol }
    syms.each { |sym| opts[sym] = nil }
    [strs.map(&:to_s), opts]
  end

  # for GMT modules which produce PostScript, convert the
  # convenience options (:position, :file) to the hash
  # equivalent of the -K, -O options, and the creation of
  # or appending to the PostScript output file

  def coerce_postscript_options!(opts)
    file = opts.delete(:file)
    position = opts.delete(:position)
    if file && position then
      file_opts =
        case position
        when :first, 'first'
          { K: nil, :> => file }
        when :middle, 'middle'
          { O: nil, K: nil, :>> => file }
        when :last, 'last'
          { O: nil, :>> => file }
        else
          raise ArgumentError, 'position should be :first, :middle or :last'
        end
      opts.merge!(file_opts)
    end
  end

  # handle the :>> option

  def coerce_append_option!(opts)
    if file = opts.delete(:>>) then
      opts[:>] = '>' + file
    end
  end

  # convert non-nil argument to string

  def string_unless_nil(arg)
    arg.to_s unless arg.nil?
  end

  # convert the options hash to an array of pairs

  def options_as_pairs(opts)
    opts.each_with_object(Array.new) do |(key, values), result|
      if values.respond_to? :each then
        values.each do |value|
          result << [key, string_unless_nil(value)]
        end
      else
        result << [key, string_unless_nil(values)]
      end
    end
  end

  public

  # @!group Plotting

  # Plot the GMT logo on maps
  wrapper_ps :gmtlogo

  # Contouring of 2-D gridded data sets
  wrapper_ps :grdcontour

  # Produce images from 2-D gridded data sets
  wrapper_ps :grdimage

  # Plotting of 2-D gridded vector fields
  wrapper_ps :grdvector

  # 3-D perspective imaging of 2-D gridded data sets
  wrapper_ps :grdview

  # Create a basemap plot
  wrapper_ps :psbasemap

  # Use polygon files to define clipping paths
  wrapper_ps :psclip

  # Plot (and fill) coastlines, borders, and rivers on maps
  wrapper_ps :pscoast

  # Contour or image raw table data by triangulation
  wrapper_ps :pscontour

  # Plot a histogram
  wrapper_ps :pshistogram

  # Plot Sun raster files on a map
  wrapper_ps :psimage

  # Plot a legend on a map
  wrapper_ps :pslegend

  # Create overlay to mask out regions on maps
  wrapper_ps :psmask

  # Plot sector or rose diagrams
  wrapper_ps :psrose

  # Plot gray scale or color scale on maps
  wrapper_ps :psscale

  # Plot text strings on maps
  wrapper_ps :pstext

  # Draw table data time-series along track on maps
  wrapper_ps :pswiggle

  # Plot symbols, polygons, and lines on maps
  wrapper_ps :psxy

  # Plot symbols, polygons, and lines in 3-D
  wrapper_ps :psxyz

  # Calculate and plot the day-night terminator
  wrapper_ps :pssolar

  # Plot data on ternary diagrams
  wrapper_ps :psternary

  # Plot event symbols and labels for a moment in time
  wrapper_ps :psevents


  # @!group Filtering

  # L2 (x,y,z) table data filter/decimator
  wrapper_other :blockmean

  # L1 (x,y,z) table data filter/decimator
  wrapper_other :blockmedian

  # Mode estimate (x,y,z) table data filter/decimator
  wrapper_other :blockmode

  # Time domain filtering of 1-D data tables
  wrapper_other :filter1d

  # Filter 2-D gridded data sets in the space domain
  wrapper_other :grdfilter

  # Directional filtering of grids in the space domain
  wrapper_other :dimfilter


  # @!group Gridding

  # Interpolation with Green’s functions for splines in 1–3 D
  wrapper_other :greenspline

  # Nearest-neighbor gridding scheme
  wrapper_other :nearneighbor

  # Spherical gridding in tension of data on a sphere
  wrapper_other :sphinterpolate

  # A continuous curvature gridding algorithm
  wrapper_other :surface

  # Perform optimal Delauney triangulation and gridding
  wrapper_other :triangulate

  # Interpolate 2-D grids or 1-D series from a 3-D data cube
  wrapper_other :grdinterpolate


  # @!group Sampling of 1-D and 2-D data

  # Line reduction using the Douglas-Peucker algorithm
  wrapper_other :gmtsimplify

  # Resample a 2-D gridded data set onto a new grid
  wrapper_other :grdsample

  # Sampling of 2-D gridded data set(s) along 1-D track
  wrapper_other :grdtrack

  # Resampling of 1-D table data sets
  wrapper_other :sample1d


  # @!group Projection and map-transformation

  # Project gridded data sets onto a new coordinate system
  wrapper_other :grdproject

  # Transformation of coordinate systems for table data
  wrapper_other :mapproject

  # Project table data onto lines or great circles
  wrapper_other :project


  # @!group Information retrieval

  # List the current default settings
  wrapper_other :gmtdefaults

  # Retrieve selected parameters in current file
  wrapper_other :gmtget

  # Get information about table data files
  wrapper_other :gmtinfo

  # Change selected parameters in current file
  wrapper_other :gmtset

  # Get information about grid files
  wrapper_other :grdinfo

  # Make selections or determine common regions from 2-D grids, images or 3-D cubes
  wrapper_other :grdselect


  # @!group Mathematical operations on tables or grids

  # Mathematical operations on table data
  wrapper_other :gmtmath

  # Make color palette tables
  wrapper_other :makecpt

  # Compute various spectral estimates from time-series
  wrapper_other :spectrum1d

  # Compute grid from spherical harmonic coefficients
  wrapper_other :sph2grd

  # Make grid of distances to nearest points on a sphere
  wrapper_other :sphdistance

  # Delaunay or Voronoi construction of spherical lon,lat data
  wrapper_other :sphtriangulate


  # @!group Convert or extract subsets of data

  # Bin spatial data and determine statistics per bin
  wrapper_other :gmtbinstats

  # Connect segments into more complete lines or polygons
  wrapper_other :gmtconnect

  # Convert data tables from one format to another
  wrapper_other :gmtconvert

  # Select subsets of table data based on multiple spatial criteria
  wrapper_other :gmtselect

  # Geospatial operations on lines and polygons
  wrapper_other :gmtspatial

  # Operations on Cartesian vectors in 2-D and 3-D
  wrapper_other :gmtvector

  # Convert Sun raster or grid file to red, green, blue component grids
  wrapper_other :grd2rgb

  # Conversion from 2-D grid file to table data
  wrapper_other :grd2xyz

  # Blend several partially over-lapping grid files onto one grid
  wrapper_other :grdblend

  # Converts grid files into other grid formats
  wrapper_other :grdconvert

  # Cut a sub-region from a grid file
  wrapper_other :grdcut

  # Paste together grid files along a common edge
  wrapper_other :grdpaste

  # Split xyz files into several segments
  wrapper_other :splitxyz

  # Convert an equidistant table xyz file to a 2-D grid file
  wrapper_other :xyz2grd

  # Create KML image quadtree from single grid
  wrapper_other :grd2kml


  # @!group Trends in 1-D and 2-D data

  # Finds the best-fitting great or small circle for a set of points
  wrapper_other :fitcircle

  # Linear regression of 1-D data sets
  wrapper_other :gmtregress

  # Fits polynomial or Fourier trends to y = f(x) series
  wrapper_other :trend1d

  # Fits polynomial trends to z = f(x,y) series
  wrapper_other :trend2d

  # Fit trend surface to grids and compute residuals
  wrapper_other :grdtrend


  # @!group Grid operations

  # Make color palette table from a grid files
  wrapper_other :grd2cpt

  # Limit the z-range in gridded data sets
  wrapper_other :grdclip

  # Modify header information in a 2-D grid file
  wrapper_other :grdedit

  # Perform operations on grid files in the frequency domain
  wrapper_other :grdfft

  # Interpolate across holes in a grid
  wrapper_other :grdfill

  # Compute directional gradient from grid files
  wrapper_other :grdgradient

  # Histogram equalization for grid files
  wrapper_other :grdhisteq

  # Create masking grid files from shoreline data base
  wrapper_other :grdlandmask

  # Reset grid nodes in/outside a clip path to constants
  wrapper_other :grdmask

  # Mathematical operations on grid files
  wrapper_other :grdmath

  # Blending and transforming grids and images
  wrapper_other :grdmix

  # Calculate volumes under a surface within specified contour
  wrapper_other :grdvolume


  # @!group Miscellaneous

  # Like psxy but plots KML for use in Google Earth
  wrapper_other :gmt2kml

  # Extracts coordinates from Google Earth KML files
  wrapper_other :kml2gmt

  # Crop and convert PostScript files to raster images, EPS, and PDF
  wrapper_other :psconvert

  # Execute GDAL raster programs from GMT
  wrapper_other :grdgdal

end

require 'gmt/gmt'
