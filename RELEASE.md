Release procedure
=================

This is all scripted in the Rakefile, just bump the
version in `gmt.gemspec`, commit and run

    bundle exec rake release
