/*
  option.c

  GMT expects arguments to be in a linked-list format and provides
  some library support for creating these.  We here convert a Ruby
  array-of-pairs to this linked-list format.
*/

#include "option.h"

static char char_of_key(VALUE key)
{
  if (TYPE(key) != T_SYMBOL)
    rb_raise(rb_eArgError, "option keys should be symbols");

  ID key_id = SYM2ID(key);

  const char *key_s = rb_id2name(key_id);

  if (key_s == NULL || *key_s == '\0')
    rb_raise(rb_eRuntimeError, "problem getting name of option");

  return *key_s;
}

static gmt_option_t* append_option(void *session,
				   char option_name,
				   const char *option_arg,
				   gmt_option_t *gmt_opts)
{
  gmt_option_t *gmt_opt = GMT_Make_Option(session, option_name, option_arg);
  return GMT_Append_Option(session, gmt_opt, gmt_opts);
}

gmt_option_t* ruby_array_to_gmt_options(VALUE opts, void *session)
{
  if (TYPE(opts) != T_ARRAY)
    rb_raise(rb_eArgError, "options should be an array");

  gmt_option_t *gmt_opts = NULL;

  for (size_t i = 0 ; i < (size_t)RARRAY_LEN(opts) ; i++)
    {
      VALUE opt = RARRAY_PTR(opts)[i];

      if (TYPE(opt) != T_ARRAY)
	rb_raise(rb_eArgError, "each option should be a array");
      if (RARRAY_LEN(opt) != 2)
	rb_raise(rb_eArgError, "each option should be a pair");

      VALUE key = RARRAY_PTR(opt)[0];
      char option_name = char_of_key(key);

      VALUE arg = RARRAY_PTR(opt)[1];
      const char *option_arg;

      switch (TYPE(arg))
	{
	case T_STRING:
	  option_arg = StringValueCStr(arg);
	  break;

	case T_NIL:
	  option_arg = NULL;
	  break;

	default:
	  rb_raise(rb_eArgError, "option argument should be string or nil");
	}

      gmt_opts = append_option(session, option_name, option_arg, gmt_opts);
    }

  return gmt_opts;
}

gmt_option_t* append_inputs_to_gmt_options(VALUE files,
                                           gmt_option_t *gmt_opts,
                                           void *session)
{
  if (TYPE(files) != T_ARRAY)
    rb_raise(rb_eArgError, "inputs should be an array");

  for (size_t i = 0 ; i < (size_t)RARRAY_LEN(files) ; i++)
    {
      VALUE file = RARRAY_PTR(files)[i];

      if (TYPE(file) != T_STRING)
	rb_raise(rb_eArgError, "each input should be a string");

      const char *option_arg = StringValueCStr(file);

      gmt_opts = append_option(session, '<', option_arg, gmt_opts);
    }

  return gmt_opts;
}
