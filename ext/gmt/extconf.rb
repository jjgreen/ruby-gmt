require 'mkmf'

$CFLAGS << ' -std=c99'

INCLUDEDIR = RbConfig::CONFIG['includedir']
HEADER_BASE = [
  '/opt/local/include',
  '/usr/local/include',
  INCLUDEDIR,
  '/usr/include',
]
HEADER_GMT = HEADER_BASE.map { |dir| File.join(dir, 'gmt') }
HEADER_DIRS = HEADER_GMT + HEADER_BASE

LIBDIR = RbConfig::CONFIG['libdir']
LIB_DIRS = [
  '/opt/local/lib',
  '/usr/local/lib',
  LIBDIR,
  '/usr/lib'
]

dir_config('gmt', HEADER_DIRS, LIB_DIRS)

abort 'missing gmt.h' unless find_header('gmt.h')
abort 'missing gmt_version.h' unless find_header('gmt_version.h')

FUNCS =
  [
    'GMT_Create_Session',
    'GMT_Destroy_Session',
  ]

abort "libgmt is missing" unless find_library('gmt', FUNCS.first)

FUNCS.each do |func|
  abort "missing #{func}()" unless have_func(func)
end

MACROS =
  [
    'GMT_MAJOR_VERSION',
    'GMT_MINOR_VERSION',
    'GMT_RELEASE_VERSION'
  ]

MACROS.each do |macro|
  abort "missing #{macro}" unless have_macro(macro, 'gmt_version.h')
end

# remove a C90 specific warning

CONFIG['warnflags'].slice!(/ -Wdeclaration-after-statement/)

create_makefile 'gmt/gmt'
