#include <ruby.h>
#include <ruby/version.h>

#include <gmt.h>
#include <gmt_version.h>

#include <errno.h>

#include "option.h"

#define API_VER(major, minor, release) (10000 * major + 100 * minor + release)
#define GMT_API_VER API_VER(GMT_MAJOR_VERSION, GMT_MINOR_VERSION, GMT_RELEASE_VERSION)
#define API_AT_LEAST(major, minor, release) (GMT_API_VER >= API_VER(major, minor, release))

typedef struct
{
  void *session;
} gmt_t;

/* helpers */

static void gmt_free(void*);

static rb_data_type_t data_type = {
  .wrap_struct_name = "ruby-gmt-wrap",
  .function = {
    .dmark = NULL,
    .dfree = gmt_free,
    .dsize = NULL,
#if RUBY_API_VERSION_CODE < 20700
    .reserved = { NULL, NULL }
#else
    .dcompact = NULL,
    .reserved = { NULL }
#endif
  },
  .parent = NULL,
  .data = NULL,
  .flags = RUBY_TYPED_FREE_IMMEDIATELY
};

/* ruby object interface */

static void gmt_free(void *p)
{
  gmt_t *gmt = p;

  if (gmt->session)
    {
      GMT_Destroy_Session(gmt->session);
      gmt->session = NULL;
    }

  free(gmt);
}

static VALUE gmt_alloc(VALUE cls)
{
  gmt_t *gmt;

  if ((gmt = malloc(sizeof(gmt_t))) == NULL)
    rb_raise(rb_eNoMemError, "failed to alloc GMT struct");

  VALUE obj = TypedData_Wrap_Struct(cls, &data_type, gmt);

  gmt->session = NULL;

  return obj;
}

static VALUE gmt_init(VALUE self)
{
  gmt_t *gmt;

  TypedData_Get_Struct(self, gmt_t, &data_type, gmt);

  gmt->session =
    GMT_Create_Session("Session name", 2, 0, NULL);

  if (gmt->session == NULL)
    rb_raise(rb_eNoMemError, "failed to create GMT session");

  return self;
}

static VALUE gmt_release(VALUE self)
{
  gmt_t *gmt;

  TypedData_Get_Struct(self, gmt_t, &data_type, gmt);

  if (gmt->session)
    {
      GMT_Destroy_Session(gmt->session);
      gmt->session = NULL;
    }

  return self;
}

/* hander for GMT functions */

static VALUE gmt_simple(const char *name, int argc, VALUE *argv, VALUE self)
{
  VALUE files, opts;
  rb_scan_args(argc, argv, "*1", &files, &opts);

  gmt_t *gmt;
  TypedData_Get_Struct(self, gmt_t, &data_type, gmt);

  if (gmt == NULL)
    rb_raise(rb_eRuntimeError, "failed fetch of GMT class data");

  void *session = gmt->session;

  gmt_option_t *gmt_opts;
  gmt_opts = ruby_array_to_gmt_options(opts, session);
  gmt_opts = append_inputs_to_gmt_options(files, gmt_opts, session);

  errno = 0;
  int err = GMT_Call_Module(session, name, -1, gmt_opts);
  int gmt_errno = errno;

  GMT_Destroy_Options(session, &gmt_opts);

  if (err != 0)
    {
      VALUE gmt_syserr;

      if ((gmt_errno != 0) &&
          ((gmt_syserr = rb_syserr_new(gmt_errno, NULL)) != 0))
        rb_exc_raise(gmt_syserr);
      else
        rb_raise(rb_eRuntimeError, "failed call to GMT module %s", name);
    }

  return Qtrue;
}

/*
  The GMT functions

  We take this list from the GMT documentation for the "classic mode"
  https://docs.generic-mapping-tools.org/latest/modules-classic.html

  The "modern mode" modules (which have a slicker way of routing and
  combining postscript output) will probably need a different treatment,
  possibly a more attractive one from the Ruby perspective; for now we
  just limit ourselves to the classic mode.
*/

#define GMT_FUN(name) \
static VALUE gmt_ ## name (int argc, VALUE *argv, VALUE self) \
{ \
  return gmt_simple(#name, argc, argv, self); \
}

/* Plotting  */

GMT_FUN(gmtlogo)
GMT_FUN(grdcontour)
GMT_FUN(grdimage)
GMT_FUN(grdvector)
GMT_FUN(grdview)
GMT_FUN(psbasemap)
GMT_FUN(psclip)
GMT_FUN(pscoast)
GMT_FUN(pscontour)
GMT_FUN(pshistogram)
GMT_FUN(psimage)
GMT_FUN(pslegend)
GMT_FUN(psmask)
GMT_FUN(psrose)
GMT_FUN(psscale)
GMT_FUN(pstext)
GMT_FUN(pswiggle)
GMT_FUN(psxy)
GMT_FUN(psxyz)

#if API_AT_LEAST(5, 3, 0)
GMT_FUN(pssolar)
#endif

#if API_AT_LEAST(5, 4, 0)
GMT_FUN(psternary)
#endif

#if API_AT_LEAST(6, 0, 0)
GMT_FUN(psevents)
#endif

/* Filtering */

GMT_FUN(blockmean)
GMT_FUN(blockmedian)
GMT_FUN(blockmode)
GMT_FUN(filter1d)
GMT_FUN(grdfilter)

#if API_AT_LEAST(6, 0, 0)
GMT_FUN(dimfilter)
#endif

/* Gridding  */

GMT_FUN(greenspline)
GMT_FUN(nearneighbor)
GMT_FUN(sphinterpolate)
GMT_FUN(surface)
GMT_FUN(triangulate)

#if API_AT_LEAST(6, 1, 0)
GMT_FUN(grdinterpolate)
#endif

/* Sampling of 1-D and 2-D data */

GMT_FUN(gmtsimplify)
GMT_FUN(grdsample)
GMT_FUN(grdtrack)
GMT_FUN(sample1d)

/* Projection and map-transformation */

GMT_FUN(grdproject)
GMT_FUN(mapproject)
GMT_FUN(project)

/* Information retrieval */

GMT_FUN(gmtdefaults)
GMT_FUN(gmtget)
GMT_FUN(gmtinfo)
GMT_FUN(gmtset)
GMT_FUN(grdinfo)

#if API_AT_LEAST(6, 3, 0)
GMT_FUN(grdselect)
#endif

/* Mathematical operations on tables or grids */

GMT_FUN(gmtmath)
GMT_FUN(makecpt)
GMT_FUN(spectrum1d)
GMT_FUN(sph2grd)
GMT_FUN(sphdistance)
GMT_FUN(sphtriangulate)

/* Convert or extract subsets of data */

GMT_FUN(gmtconnect)
GMT_FUN(gmtconvert)
GMT_FUN(gmtselect)
GMT_FUN(gmtspatial)
GMT_FUN(gmtvector)
GMT_FUN(grd2xyz)
GMT_FUN(grdblend)
GMT_FUN(grdconvert)
GMT_FUN(grdcut)
GMT_FUN(grdpaste)
GMT_FUN(splitxyz)
GMT_FUN(xyz2grd)

#if API_AT_LEAST(6, 0, 0)
GMT_FUN(grd2kml)
#else
GMT_FUN(grd2rgb) /* https://github.com/GenericMappingTools/gmt/pull/425 */
#endif

#if API_AT_LEAST(6, 2, 0)
GMT_FUN(gmtbinstats)
#endif

/* trends in 1-D and 2-D data */

GMT_FUN(fitcircle)
GMT_FUN(gmtregress)
GMT_FUN(trend1d)
GMT_FUN(trend2d)

#if API_AT_LEAST(6, 0, 0)
GMT_FUN(grdtrend)
#endif

/* Grid operations */

GMT_FUN(grd2cpt)
GMT_FUN(grdclip)
GMT_FUN(grdedit)
GMT_FUN(grdfft)
GMT_FUN(grdgradient)
GMT_FUN(grdhisteq)
GMT_FUN(grdlandmask)
GMT_FUN(grdmask)
GMT_FUN(grdmath)
GMT_FUN(grdvolume)

#if API_AT_LEAST(6, 0, 0)
GMT_FUN(grdfill)
#endif

#if API_AT_LEAST(6, 1, 0)
GMT_FUN(grdmix)
#endif

/* Miscellaneous  */

GMT_FUN(gmt2kml)
GMT_FUN(kml2gmt)
GMT_FUN(psconvert)

#if API_AT_LEAST(6, 1, 0)
GMT_FUN(grdgdal)
#endif

#undef GMT_FUN

#define RB_DPM(name) rb_define_private_method(cGMT, #name "_c", gmt_ ## name, -1)

void Init_gmt(void)
{
  VALUE cGMT = rb_const_get(rb_cObject, rb_intern("GMT"));

  rb_define_alloc_func(cGMT, gmt_alloc);
  rb_define_method(cGMT, "initialize", gmt_init, 0);
  rb_define_method(cGMT, "free", gmt_release, 0);

  /* Plotting */

  RB_DPM(gmtlogo);
  RB_DPM(grdcontour);
  RB_DPM(grdimage);
  RB_DPM(grdvector);
  RB_DPM(grdview);
  RB_DPM(psbasemap);
  RB_DPM(psclip);
  RB_DPM(pscoast);
  RB_DPM(pscontour);
  RB_DPM(pshistogram);
  RB_DPM(psimage);
  RB_DPM(pslegend);
  RB_DPM(psmask);
  RB_DPM(psrose);
  RB_DPM(psscale);
  RB_DPM(pstext);
  RB_DPM(pswiggle);
  RB_DPM(psxy);
  RB_DPM(psxyz);

#if API_AT_LEAST(5, 3, 0)
  RB_DPM(pssolar);
#endif

#if API_AT_LEAST(5, 4, 0)
  RB_DPM(psternary);
#endif

#if API_AT_LEAST(6, 0, 0)
  RB_DPM(psevents);
#endif

  /* Filtering  */

  RB_DPM(blockmean);
  RB_DPM(blockmedian);
  RB_DPM(blockmode);
  RB_DPM(filter1d);
  RB_DPM(grdfilter);

#if API_AT_LEAST(6, 0, 0)
  RB_DPM(dimfilter);
#endif

  /* Gridding */

  RB_DPM(greenspline);
  RB_DPM(nearneighbor);
  RB_DPM(sphinterpolate);
  RB_DPM(surface);
  RB_DPM(triangulate);

#if API_AT_LEAST(6, 1, 0)
  RB_DPM(grdinterpolate);
#endif

  /* Sampling of 1-D and 2-D data */

  RB_DPM(gmtsimplify);
  RB_DPM(grdsample);
  RB_DPM(grdtrack);
  RB_DPM(sample1d);

  /* Projection and map-transformation */

  RB_DPM(grdproject);
  RB_DPM(mapproject);
  RB_DPM(project);

  /* Information retrieval */

  RB_DPM(gmtdefaults);
  RB_DPM(gmtget);
  RB_DPM(gmtinfo);
  RB_DPM(gmtset);
  RB_DPM(grdinfo);

#if API_AT_LEAST(6, 3, 0)
  RB_DPM(grdselect);
#endif

  /* Mathematical operations on tables or grids */

  RB_DPM(gmtmath);
  RB_DPM(makecpt);
  RB_DPM(spectrum1d);
  RB_DPM(sph2grd);
  RB_DPM(sphdistance);
  RB_DPM(sphtriangulate);

  /* Convert or extract subsets of data */

#if API_AT_LEAST(6, 2, 0)
  RB_DPM(gmtbinstats);
#endif

  RB_DPM(gmtconnect);
  RB_DPM(gmtconvert);
  RB_DPM(gmtselect);
  RB_DPM(gmtspatial);
  RB_DPM(gmtvector);
  RB_DPM(grd2xyz);
  RB_DPM(grdblend);
  RB_DPM(grdconvert);
  RB_DPM(grdcut);
  RB_DPM(grdpaste);
  RB_DPM(splitxyz);
  RB_DPM(xyz2grd);

#if API_AT_LEAST(6, 0, 0)
  RB_DPM(grd2kml);
#else
  RB_DPM(grd2rgb); /* https://github.com/GenericMappingTools/gmt/pull/425 */
#endif

  /* Trends in 1-D and 2-D data */

  RB_DPM(fitcircle);
  RB_DPM(gmtregress);
  RB_DPM(trend1d);
  RB_DPM(trend2d);

#if API_AT_LEAST(6, 0, 0)
  RB_DPM(grdtrend);
#endif

  /* Grid operations */

  RB_DPM(grd2cpt);
  RB_DPM(grdclip);
  RB_DPM(grdedit);
  RB_DPM(grdfft);
  RB_DPM(grdgradient);
  RB_DPM(grdhisteq);
  RB_DPM(grdlandmask);
  RB_DPM(grdmask);
  RB_DPM(grdmath);
  RB_DPM(grdvolume);

#if API_AT_LEAST(6, 0, 0)
  RB_DPM(grdfill);
#endif

#if API_AT_LEAST(6, 1, 0)
  RB_DPM(grdmix);
#endif

  /* Miscellaneous  */

  RB_DPM(gmt2kml);
  RB_DPM(kml2gmt);
  RB_DPM(psconvert);

#if API_AT_LEAST(6, 1, 0)
  RB_DPM(grdgdal);
#endif

  /* version constants */

  rb_define_const(cGMT, "VERSION_MAJOR", INT2NUM(GMT_MAJOR_VERSION));
  rb_define_const(cGMT, "VERSION_MINOR", INT2NUM(GMT_MINOR_VERSION));
  rb_define_const(cGMT, "VERSION_RELEASE", INT2NUM(GMT_RELEASE_VERSION));
}

#undef RB_DPM
