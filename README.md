ruby-gmt
--------

An unofficial Ruby native extension for [GMT][1]: the Generic
Mapping Tools. This is an incomplete but usable subset of the
GMT API for versions 5 and 6.

[1]: https://docs.generic-mapping-tools.org/
