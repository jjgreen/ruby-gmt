Gem::Specification.new do |s|
  s.name = 'gmt'
  s.description = <<~EOF
    A Ruby extension for the Generic Mapping Tools (GMT5/6)
    cartographic toolset.
  EOF
  s.version = '0.2.5'
  s.summary = 'Generic Mapping Tools (GMT)'
  s.authors = ['J.J. Green']
  s.licenses = ['MIT']
  s.email = 'j.j.green@gmx.co.uk'
  s.homepage = 'https://gitlab.com/jjg/ruby-gmt'
  s.platform = Gem::Platform::RUBY
  s.files = [
    'lib/gmt.rb',
    'ext/gmt/gmt.c',
    'ext/gmt/option.c',
    'ext/gmt/option.h',
  ]
  s.require_paths = [ 'lib', 'ext' ]
  s.extensions = ['ext/gmt/extconf.rb']
  s.add_development_dependency 'bundler', ['~> 2']
  s.add_development_dependency 'rake', ['~> 13.0']
  s.add_development_dependency 'ostruct', ['~> 0.6']
  s.add_development_dependency 'rspec', ['~> 3.6']
  s.add_development_dependency 'yard', ['~> 0.9']
  s.add_development_dependency 'rubygems-tasks', ['~> 0.2.4']
  s.add_development_dependency 'rake-compiler', ['~> 1']
  s.requirements << 'GMT5/6 development libraries (compile)'
  s.requirements << 'GMT5/6 installation (test)'
  s.requirements << 'ghostscript (test)'
end
