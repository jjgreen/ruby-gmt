RSpec::Matchers.define :be_valid_cpt do
  match do |path|
    if File.exist? path then
      command = format('gmt makecpt -C"%s" > /dev/null', path)
      system(command)
    end
  end

  failure_message do |path|
    "expected #{path} to be valid cpt"
  end

  failure_message_when_negated do |path|
    "expected #{path} not to be vaild cpt"
  end

  description do
    "checks a file is valid cpt"
  end
end
