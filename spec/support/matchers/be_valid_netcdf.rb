RSpec::Matchers.define :be_valid_netcdf do
  match do |path|
    if File.exist? path then
      command = format('gmt grdinfo "%s" > /dev/null', path)
      system(command)
    end
  end

  failure_message do |path|
    "expected #{path} to be valid netcdf"
  end

  failure_message_when_negated do |path|
    "expected #{path} not to be vaild netcdf"
  end

  description do
    "checks a file is valid netcdf"
  end
end
