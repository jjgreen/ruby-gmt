RSpec::Matchers.define :be_valid_postscript do
  match do |path|
    if File.exist? path then
      command =
        format('gs -sDEVICE=nullpage -dNOPAUSE -dBATCH "%s" > /dev/null', path)
      system(command)
    end
  end

  failure_message do |path|
    "expected #{path} to be valid postscript"
  end

  failure_message_when_negated do |path|
    "expected #{path} not to be vaild postscript"
  end

  description do
    "checks a file is valid postscript"
  end
end
