describe 'The version constants' do

  describe 'VERSION_MAJOR' do
    let(:version_major) { GMT::VERSION_MAJOR }

    it 'is an integer' do
      expect(version_major).to be_an Integer
    end

    it 'is non-negative' do
      expect(version_major).to be >= 0
    end
  end

  describe 'VERSION_MINOR' do
    let(:version_minor) { GMT::VERSION_MINOR }

    it 'is an integer' do
      expect(version_minor).to be_an Integer
    end

    it 'is non-negative' do
      expect(version_minor).to be >= 0
    end
  end

  describe 'VERSION_RELEASE' do
    let(:version_release) { GMT::VERSION_RELEASE }

    it 'is an integer' do
      expect(version_release).to be_an Integer
    end

    it 'is non-negative' do
      expect(version_release).to be >= 0
    end
  end

  describe '.version_at_least' do

    context 'for the previous major version' do
      let(:major_version) { GMT::VERSION_MAJOR - 1 }

      it 'is true' do
        expect(GMT.version_at_least? major_version).to eq true
      end
    end

    context 'for the next major version' do
      let(:major_version) { GMT::VERSION_MAJOR + 1 }

      it 'is false' do
        expect(GMT.version_at_least? major_version).to eq false
      end
    end

    context 'for current major version' do
      let(:major_version) { GMT::VERSION_MAJOR }
      let(:result) do
        GMT.version_at_least?(major_version, minor_version)
      end

      context 'for the previous minor version' do
        let(:minor_version) { GMT::VERSION_MINOR - 1 }

        it 'is true' do
          expect(result).to eq true
        end
      end

      context 'for the next minor version' do
        let(:minor_version) { GMT::VERSION_MINOR + 1 }

        it 'is false' do
          expect(result).to eq false
        end
      end

      context 'for current minor version' do
        let(:minor_version) { GMT::VERSION_MINOR }
        let(:result) do
          GMT.version_at_least?(major_version, minor_version, release_version)
        end

        context 'for the previous release version' do
          let(:release_version) { GMT::VERSION_RELEASE - 1 }

          it 'is true' do
            expect(result).to eq true
          end
        end

        context 'for the next release version' do
          let(:release_version) { GMT::VERSION_RELEASE + 1 }

          it 'is false' do
            expect(result).to eq false
          end
        end

        context 'for the current release version' do
          let(:release_version) { GMT::VERSION_RELEASE }

          it 'is true' do
            expect(result).to eq true
          end
        end
      end
    end
  end
end
