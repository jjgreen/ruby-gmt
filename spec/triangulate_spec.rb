describe 'The triangulate module' do
  let(:gmt) { GMT.new }
  let(:fixture_path) { fixture('spectrum.csv') }

  describe 'input argument' do

    context 'string' do

      it 'creates a file' do
        with_temporary_file('processed.dat') do |dat|
          gmt.triangulate(fixture_path, :> => dat)
          expect(File.exist? dat).to eq true
        end
      end
    end

    context 'pathname' do

      # Regression -- failed in 0.1.5

      it 'creates a file' do
        with_temporary_file('processed.dat') do |dat|
          gmt.triangulate(Pathname(fixture_path), :> => dat)
          expect(File.exist? dat).to eq true
        end
      end
    end

    context 'an object without the :to_s method' do

      let(:unstringable_class) do
        Class.new(Object) do
          attr_reader :path
          def initialize(path)
            @path = path
          end
          undef_method :to_s
        end
      end

      let(:unstringable) { unstringable_class.new('/no/such') }

      before { expect(unstringable).to_not respond_to(:to_s) }

      it 'raises NoMethodError' do
        with_temporary_file('processed.dat') do |dat|
          expect {
            gmt.triangulate(unstringable, :> => dat)
          }.to raise_error(NoMethodError, /to_s/)
        end
      end
    end
  end

end
