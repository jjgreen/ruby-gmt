describe 'The psrose module' do
  let(:gmt) { GMT.new }
  let(:options) do
    {
      R: '0/125/0/360',
      S: '5c',
      B: ['xg25', 'yg45', '+t"Windrose diagram"'],
      W: '2.5p',
      U: 'c'
    }
  end

  it 'creates valid PostScript' do
    with_temporary_file('psrose.ps') do |ps|
      gmt.psrose(fixture('rose.txt'), :> => ps, **options)
      expect(ps).to be_valid_postscript
    end
  end
end
