describe 'The grdcontour module' do
  let(:gmt) { GMT.new }
  let(:options) do
    { J: 'X5i', C: '0.1', B: 'a0.5f0.1' }
  end

  it 'creates valid PostScript' do
    with_temporary_file('grdcontour.ps') do |ps|
      gmt.grdcontour(fixture('grid.grd'), :> => ps, **options)
      expect(ps).to be_valid_postscript
    end
  end
end
