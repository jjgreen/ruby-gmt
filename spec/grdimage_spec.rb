describe 'The grdimage module' do
  let(:gmt) { GMT.new }
  let(:options) do
    { J: 'X5i', C: 'polar', B: 'a0.5f0.1' }
  end

  it 'creates valid PostScript' do
    with_temporary_file('grdimage.ps') do |ps|
      gmt.grdimage(fixture('grid.grd'), :> => ps, **options)
      expect(ps).to be_valid_postscript
    end
  end
end
