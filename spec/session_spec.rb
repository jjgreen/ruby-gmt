describe 'The GMT class' do

  it 'has a :new method' do
    GMT.new
  end

  describe 'the instance' do
    let(:gmt) { GMT.new }

    it 'is a GMT' do
      expect(gmt).to be_a GMT
    end

    it 'responds to :free' do
      expect(gmt).to respond_to :free
    end
  end
end

describe 'the session class method' do

  it 'yields a GMT object' do
    GMT.session do |gmt|
      expect(gmt).to be_a GMT
    end
  end

  it 'can create a plot' do
    with_temporary_file('gmtlogo.ps') do |ps|
      GMT.session do |gmt|
        gmt.gmtlogo(:P, D: 'x0/0+w2i', :> => ps)
      end
      expect(ps).to be_valid_postscript
    end
  end
end
