describe 'The gmtlogo module' do
  let(:gmt) { GMT.new }

  context 'with a nil-valued option' do

    it 'creates valid PostScript' do
      with_temporary_file('gmtlogo.ps') do |ps|
        gmt.gmtlogo(P: nil, D: 'x0/0+w2i', :> => ps)
        expect(ps).to be_valid_postscript
      end
    end
  end

  context 'with a symbol argument' do

    it 'creates valid PostScript' do
      with_temporary_file('gmtlogo.ps') do |ps|
        gmt.gmtlogo(:P, D: 'x0/0+w2i', :> => ps)
        expect(ps).to be_valid_postscript
      end
    end
  end
end
