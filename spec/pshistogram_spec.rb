describe 'The pshistogram module' do
  let(:gmt) { GMT.new }

  context 'with a nil option' do

    it 'creates valid PostScript' do
      with_temporary_file('pshistogram.ps') do |ps|
        gmt.pshistogram(
          fixture('histogram.txt'),
          F: nil, J: 'X3i', W: '1', L: '0.5p',
          :> => ps
        )
        expect(ps).to be_valid_postscript
      end
    end
  end

  context 'with a symbol argument' do

    it 'creates valid PostScript' do
      with_temporary_file('pshistogram.ps') do |ps|
        gmt.pshistogram(
          fixture('histogram.txt'),
          :F, J: 'X3i', W: '1', L: '0.5p',
          :> => ps
        )
        expect(ps).to be_valid_postscript
      end
    end
  end
end
